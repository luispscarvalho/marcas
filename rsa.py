import Crypto
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_v1_5

from Crypto import Random

import base64

import urllib.parse

# gera uma chave privada
random_generator = Random.new().read
privatekey = RSA.generate(1024, random_generator)
# exporta chave privada para arquivo (deve ser usado pelos servicos)
f = open('privatekey', 'wb')
f.write(privatekey.exportKey())

# gera uma chave publica (usada pela app) a partir da chave privada
publickey = privatekey.publickey()
print('\npublic key:\n\n' + str(publickey.exportKey()))

# recupera chave privada e cria um cifrador
f = open('privatekey', 'rb')
privatekey = f.read()
privatekey = RSA.importKey(privatekey)
cipher = PKCS1_v1_5.new(privatekey)

# teste
# encripta uma informacao
AUTH_KEY = b'on second thought, lets not go to kamelot. Tiss a silly place'
encrypted = cipher.encrypt(AUTH_KEY)
print('\nencrypted:\n\n' + str(encrypted))

decrypted = cipher.decrypt(encrypted, None)
print('\ndecrypted:\n\n' + str(decrypted))

