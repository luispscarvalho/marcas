from flask import Flask
from flask import request, jsonify, json

from pymongo import MongoClient
from bson.json_util import dumps
from bson.objectid import ObjectId

import urllib.parse as urllib
from utils import *

service = Flask(__name__)

VERSION = "1.0"

IS_ALIVE = True
DEBUG = True


@service.route("/v" + VERSION + "/isalive")
def isAlive():
    has, error = hasError(IS_ALIVE, request)

    return jsonify(
        isAlive= 'no' if has else 'yes'
    )


@service.route("/v" + VERSION + "/countlikes/<string:jsonified>")
def countLikes(jsonified):
    count = 0

    has, error = hasError(IS_ALIVE, request)
    if not has:
        feed = json.loads(jsonified)

        count = countLikesByFeed(ObjectId(feed['feedId']))
        error = ERROR_NONE()

    return jsonify(
        count=count,
        error=error
    )


@service.route("/v" + VERSION + "/likedby/<string:jsonified>")
def likedByUser(jsonified):
    likeId = None

    has, error = hasError(IS_ALIVE, request)
    if not has:
        coll = getLikes()

        liked = json.loads(jsonified)

        userId = urllib.unquote(liked['userId'])
        error, userId = decrypt(userId)

        if not error['id']:
            like = coll.find_one({'$and': [{'feed': ObjectId(liked['feedId'])}, {
                                'user': ObjectId(userId)}]}, {'_id': 1})
            if like:
                likeId = str(like['_id'])

    return jsonify(
        likeId=likeId,
        error=error
    )


@service.route("/v" + VERSION + "/like/<string:jsonified>")
def like(jsonified):
    result = 'failed'

    has, error = hasError(IS_ALIVE, request)
    if not has:
        coll = getLikes()

        like = json.loads(jsonified)

        userId = urllib.unquote(like['userId'])
        error, userId = decrypt(userId)

        if not error['id']:
            result = coll.insert_one(
                {'feed': ObjectId(like['feedId']), 'user': ObjectId(userId)})
            if (result.inserted_id):
                result = 'ok'

    return jsonify(
        result=result,
        error=error
    )


@service.route("/v" + VERSION + "/dislike/<string:jsonified>")
def dislike(jsonified):
    result = 'failed'

    has, error = hasError(IS_ALIVE, request)
    if not has:
        dislike = json.loads(jsonified)

        coll = getLikes()
        result = coll.delete_one({'_id': ObjectId(dislike['likeId'])})
        if (result.deleted_count):
            result = 'ok'

    return jsonify(
        result=result,
        error=error
    )


if __name__ == '__main__':
    service.run(
        host='0.0.0.0',
        debug=DEBUG)
