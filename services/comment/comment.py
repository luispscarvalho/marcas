from flask import Flask
from flask import request, jsonify, json

from pymongo import MongoClient
from bson.json_util import dumps
from bson.objectid import ObjectId

import urllib.parse as urllib
from utils import *

service = Flask(__name__)

VERSION = "1.0"

IS_ALIVE = True
DEBUG = True
PAGE_SIZE = 4


@service.route('/v' + VERSION + '/<string:jsonified>')
def comments(jsonified):
    comments = []

    has, error = hasError(IS_ALIVE, request)
    if not has:
        parameters = json.loads(jsonified)

        userId = urllib.unquote(parameters['userId'])
        error, userId = decrypt(userId)

        if not error['id']:
            coll = getComments()
            comments = coll.find({'feed': ObjectId(
                parameters['feedId'])}, skip=parameters['page'] * PAGE_SIZE, limit=PAGE_SIZE).sort("datetime", -1)
            comments = toJson(comments)

            for comment in comments:
                commentUserId = str(comment['user']['userId']['$oid'])
                comment['postedByUser'] = (commentUserId == userId)


    return jsonify(
        comments=comments,
        error=error
    )


@service.route("/v" + VERSION + "/add/<string:jsonified>")
def add(jsonified):
    result = 'failed'

    has, error = hasError(IS_ALIVE, request)
    if not has:
        comment = json.loads(jsonified)

        userId = urllib.unquote(comment['userId'])
        error, userId = decrypt(userId)

        if not error['id']:
            user = getUsers().find_one(ObjectId(userId))

            coll = getComments()
            result = coll.insert_one({
                "feed": ObjectId(comment['feedId']),
                "user": {"userId": ObjectId(userId), "name": user["name"]},
                "datetime": getDatetime(),
                "content": comment['content']
            })

            if (result.inserted_id):
                result = 'ok'

    return jsonify(
        result=result,
        error=error
    )


@service.route("/v" + VERSION + "/remove/<string:jsonified>")
def remove(jsonified):
    result = 'failed'

    has, error = hasError(IS_ALIVE, request)
    if not has:
        comment = json.loads(jsonified)

        coll = getComments()
        result = coll.delete_one({'_id': ObjectId(comment['commentId'])})
        if (result.deleted_count):
            result = 'ok'

    return jsonify(
        result=result,
        error=error
    )


if __name__ == '__main__':
    service.run(
        host='0.0.0.0',
        debug=DEBUG
    )
