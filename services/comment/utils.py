import Crypto
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_v1_5

from flask import request, json, jsonify
from bson.objectid import ObjectId
from bson.json_util import dumps
from pymongo import MongoClient
from datetime import datetime
import urllib.parse as urllib
import base64

from URLS import DATABASE_HOST, DATABASE_PORT

keyFile = open("privatekey", "rb")
privateKey = keyFile.read()
privateKey = RSA.importKey(privateKey)

TOKEN = "On second thought, let's not go to Kamelot. It's a silly place!"

def ERROR_NONE(): return {'id': 0, 'message': ''}


def ERROR_NOT_ALIVE(): return {
    'id': 1, 'message': 'Em manutenção. Informações não disponíveis'}


def ERROR_NOT_AUTHORIZED(): return {
    'id': 2, 'message': 'Você não está autorizado'}


def ERROR_UNABLE_TO_COMPLETE(): return {
    'id': 3, 'message': 'Não foi possível completar a operação'}


def ERROR_OTHER(error): return {
    'id': 100, 'message': error}


def FAKE_ID(): return 'm&2on)tq4i46(mtiqa!h8t41#)ak0cla^dw31*0a1)sp@0!x@'


def getDBConnection():
    conn = MongoClient(DATABASE_HOST, DATABASE_PORT)

    return conn


def getUsers():
    conn = getDBConnection()
    coll = conn.database.users

    return coll


def getFeeds():
    conn = getDBConnection()
    coll = conn.database.feeds

    return coll


def getCompanies():
    conn = getDBConnection()
    coll = conn.database.companies

    return coll


def getLikes():
    conn = getDBConnection()
    coll = conn.database.likes

    return coll


def getComments():
    conn = getDBConnection()
    coll = conn.database.comments

    return coll


def getDatetime():
    now = datetime.now()

    return now.strftime('%Y-%m-%d %H:%M:%S')


def toJson(documents):
    return json.loads(dumps(documents))


def countLikesByFeed(feedId):
    count = 0

    coll = getLikes()
    if (coll.count()):
        likes = coll.find({'feed': feedId})
        if (likes):
            count = likes.count()

    return count


def decrypt(encrypted):
    error = ERROR_NONE()
    decrypted = ""

    try:
        cipher = PKCS1_v1_5.new(privateKey)

        encrypted = base64.b64decode(encrypted)
        decrypted = cipher.decrypt(encrypted, None)
        decrypted = str(decrypted, 'utf-8')
    except Exception as e:
        # error = ERROR_OTHER('erro: ' + str(e))
        error = ERROR_UNABLE_TO_COMPLETE()

        pass

    return error, decrypted

def encrypt(information):
    error = ERROR_NONE()
    encrypted = ""

    try:
        cipher = PKCS1_v1_5.new(privateKey)

        encrypted = str.encode(information)
        encrypted = cipher.encrypt(encrypted)
        encrypted = base64.b64encode(encrypted)
        encrypted = encrypted.decode('utf-8')
    except Exception as e:
        # error = ERROR_OTHER('erro: ' + str(e))
        error = ERROR_UNABLE_TO_COMPLETE()

        pass

    return error, encrypted


def hasError(isAlive, request):
    has = False
    error = ERROR_NONE()

    if not isAlive:
        error = ERROR_NOT_ALIVE()
    else:
        token = request.headers.get('Authorization')
        error, token = decrypt(token)
        if not error['id']:
            if token != TOKEN:
                error = ERROR_NOT_AUTHORIZED()

    return (error['id'] > 0), error
