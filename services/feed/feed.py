from URLS import IMAGES_SERVER_URL

from flask import Flask
from flask import request, jsonify, json, redirect

from pymongo import MongoClient
from bson.json_util import dumps
from bson.objectid import ObjectId

import urllib.parse as urllib
from utils import *

service = Flask(__name__)

VERSION = "1.0"

IS_ALIVE = True
DEBUG = True
PAGE_SIZE = 4


def fillCompanyImagesURL(company):
    avatar = company['avatar']
    avatar = avatar.replace(
        '%REPLACE_WITH_IMAGES_SERVER_URL%', IMAGES_SERVER_URL)

    company['avatar'] = avatar

    return company


def fillFeedImagesURL(feed):
    blobs = feed['product']['blobs']

    for blob in blobs:
        bfile = blob['file']
        bfile = bfile.replace(
            '%REPLACE_WITH_IMAGES_SERVER_URL%', IMAGES_SERVER_URL)

        blob['file'] = bfile

    return feed


def fillCompany(feed):
    companies = getCompanies()

    companyId = feed['company']
    company = companies.find_one(filter={'_id': companyId})

    feed['company'] = fillCompanyImagesURL(company)

    return feed


def fillLikes(feed):
    feed['likes'] = countLikesByFeed(feed['_id'])

    return feed


def fillExtraData(feeds):
    filled = []

    for feed in feeds:
        filled.append(fillLikes(fillCompany(fillFeedImagesURL(feed))))

    return filled


def toMongoQuery(searchFor, companies, likedByUser, userId):
    error = ERROR_NONE()
    query = None

    if (searchFor):
        product = searchFor['product']
        if (product):
            query = {'product.name': {
                '$regex': '.*' + searchFor['product'] + '.*', '$options': 'i'}}

    if (companies):
        oids = []
        for oid in companies['oids']:
            oids.append(ObjectId(oid))

        if len(oids):
            if (query):
                query = {'$and': [query, {'company': {'$in': oids}}]}
            else:
                query = {'company': {'$in': oids}}

    if (likedByUser == '1'):
        coll = getLikes()

        userId = urllib.unquote(userId)
        error, userId = decrypt(userId)

        if not error['id']:
            feeds = coll.find({'user': ObjectId(userId)}, {'feed': 1})

            oids = []
            for feed in feeds:
                oids.append(feed['feed'])

            if (query):
                query = {'$and': [query, {'_id': {'$in': oids}}]}
            else:
                query = {'_id': {'$in': oids}}

    return error, query


def getAllCompanies():
    companies = []

    docs = getCompanies().find()
    for company in docs:
        companies.append(fillCompanyImagesURL(company))

    return companies


@service.route("/v" + VERSION + "/<string:jsonified>")
def feeds(jsonified):
    companies = []
    feeds = []

    has, error = hasError(IS_ALIVE, request)
    if not has:
        companies = toJson(getAllCompanies())

        parameters = json.loads(jsonified)
        error, query = toMongoQuery(parameters['searchFor'], parameters['companies'],
                              parameters['likedByUser'], parameters['userId'])

        if not error['id']:
            coll = getFeeds()
            if (query):
                feeds = coll.find(query, skip=parameters['page'] * PAGE_SIZE,
                                limit=PAGE_SIZE).sort("datetime", -1)
            else:
                feeds = coll.find(skip=parameters['page'] * PAGE_SIZE,
                                limit=PAGE_SIZE).sort("datetime", -1)

            feeds = toJson(fillExtraData(feeds))

    return jsonify(
        companies=companies,
        feeds=feeds,
        error=error
    )


@service.route("/v" + VERSION + "/find/<string:jsonified>")
def find(jsonified):
    has, error = hasError(IS_ALIVE, request)
    if not has:
        coll = getFeeds()

        feed = json.loads(jsonified)
        feed = coll.find_one(ObjectId(feed['feedId']))
        if feed:
            feed = toJson(fillCompany(fillFeedImagesURL(feed)))
        else:
            feed = {}

    return jsonify(
        feed=feed,
        error=ERROR_NONE()
    )


if __name__ == '__main__':
    service.run(
        host='0.0.0.0',
        debug=DEBUG)
