from flask import Flask
from flask import request, jsonify, json
import urllib.parse as urllib

from utils import *

service = Flask(__name__)

VERSION = "1.0"

IS_ALIVE = True
DEBUG = True


def add(user):
    error = ERROR_UNABLE_TO_COMPLETE()

    user['authorized'] = True

    coll = getUsers()
    userId = coll.insert_one(user).inserted_id
    if (userId):
        userId = str(userId)
        error = ERROR_NONE()

    return error, userId


@service.route("/v" + VERSION + "/authorize/<string:jsonified>")
def authorize(jsonified):
    userId = None

    has, error = hasError(IS_ALIVE, request)
    if not has:
        user = json.loads(jsonified)

        error, name = decrypt(urllib.unquote(user['name']))
        if not error['id']:
            error, account = decrypt(urllib.unquote(user['account']))
            if not error['id']:
                user['name'] = name
                user['account'] = account

                coll = getUsers()
                found = coll.find_one({'account': user['account']})

                if found:
                    if (found['authorized']):
                        userId = str(found['_id'])
                else:
                    error, userId = add(user)

                if userId:
                    error, userId = encrypt(userId)

    return jsonify(userId=userId, error=error)


if __name__ == '__main__':
    service.run(
        host='0.0.0.0',
        debug=DEBUG)
