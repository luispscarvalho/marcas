import Crypto
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_v1_5

from flask import jsonify

keyFile = open("privatekey", "rb")
privateKey = keyFile.read()
privateKey = RSA.importKey(privateKey)
cipher = PKCS1_v1_5.new(privateKey)

information = '123456789'

encrypted = str.encode(information)
encrypted = cipher.encrypt(encrypted)
print(encrypted)
# encrypted = str(encrypted) #.decode('utf-8')

import base64
encrypted = base64.b64encode(encrypted)
encrypted = encrypted.decode('utf-8')
print(encrypted)

encrypted = 'U4J7LVw0TJ3pUiuyGcji4PJ3RGdIJAczcjdTlPfsBExz/GRenj6BY7f73UBtGA3eJ0X5e/IqwGDl1FmBdmFaD7KYvRmG2cYkC6VKtWXM5gqEKuB3Ivm0DwMmHb53knAgtXaIBjeVWuOA9J1dbFq7PG6LOkKKxyqF+iQHswLnH8A='

encrypted = base64.b64decode(encrypted)
encrypted = cipher.decrypt(encrypted, None)
print(encrypted)

# import binascii
# encrypted = binascii.hexlify(encrypted)

# print(encrypted)

# encrypted = binascii.unhexlify(encrypted)
# encrypted = cipher.decrypt(encrypted, None)

# print(encrypted.decode('utf-8'))