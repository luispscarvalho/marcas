from URLS import DATABASE_HOST, DATABASE_PORT

from pymongo import MongoClient, ASCENDING, DESCENDING
from bson.objectid import ObjectId
from bson.json_util import dumps

import random

conn = MongoClient(DATABASE_HOST, DATABASE_PORT)

IMAGES_SERVER_URL = '%REPLACE_WITH_IMAGES_SERVER_URL%'
# create database
db = conn.database


companies = db.companies
chillibeans = companies.insert(
    {
        "name": "Chilli Beans",
        "home": "https://loja.chillibeans.com.br/",
        "facebook": "https://www.facebook.com/ChilliBeansBR",
        "instagram": "https://www.instagram.com/chillibeansoficial/",
        "avatar": IMAGES_SERVER_URL + "chillibeans.jpeg"
    })
rayban = companies.insert(
    {
        "name": "Ray-Ban",
        "home": "https://www.ray-ban.com/brazil",
        "facebook": "https://www.facebook.com/RayBan",
        "instagram": "https://www.instagram.com/rayban/",
        "avatar": IMAGES_SERVER_URL + "rayban.jpeg"
    })
hstern = companies.insert(
    {
        "name": "H.Stern",
        "home": "https://www.hstern.com.br/",
        "facebook": "https://www.facebook.com/hsternofficial",
        "instagram": "https://www.instagram.com/hsternofficial/",
        "avatar": IMAGES_SERVER_URL + "hstern.jpeg"
    }
)
boticario = companies.insert(
    {
        "name": "Boticario",
        "home": "https://www.boticario.com.br/",
        "facebook": "https://www.facebook.com/oboticario",
        "instagram": "https://www.instagram.com/oboticario/",
        "avatar": IMAGES_SERVER_URL + "boticario.jpeg"
    }
)
arezzo = companies.insert(
    {
        "name": "Arezzo",
        "home": "https://www.arezzo.com.br/",
        "facebook": "https://pt-br.facebook.com/arezzo.oficial",
        "instagram": "https://www.instagram.com/AREZZO/",
        "avatar": IMAGES_SERVER_URL + "arezzo.jpeg"
    }
)
daffiti = companies.insert(
    {
        "name": "Dafiti",
        "home": "https://www.dafiti.com.br/",
        "facebook": "https://www.facebook.com/Dafiti",
        "instagram": "https://www.instagram.com/dafiti/",
        "avatar": IMAGES_SERVER_URL + "daffiti.jpeg"
    }
)

FEEDS = [
    {
        "datetime": "2019-11-01T12:00-0500",
        "company": chillibeans,
        "likes": 0,
        "product": {
            "name": "Oculos Esportivo",
            "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            "price": 100.00,
            "url": "https://loja.chillibeans.com.br/",
            "blobs": [
                {
                    "type": "image",
                    "file": IMAGES_SERVER_URL + "glasses0.jpeg"
                },
                {
                    "type": "image",
                    "file": IMAGES_SERVER_URL + "glasses1.jpeg"
                },
                {
                    "type": "image",
                    "file": IMAGES_SERVER_URL + "glasses2.jpeg"
                }
            ]
        }
    },

    {
        "datetime": "2019-10-01T12:00-0500",
        "company": rayban,
        "likes": 0,
        "product": {
            "name": "Oculos Esportivo",
            "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            "price": 110.00,
            "url": "https://loja.chillibeans.com.br/",
            "blobs": [
                {
                    "type": "image",
                    "file": IMAGES_SERVER_URL + "glasses0.jpeg"
                },
                {
                    "id": "blob5",
                    "type": "image",
                    "file": IMAGES_SERVER_URL + "glasses1.jpeg"
                }
            ]
        }
    },

    {
        "datetime": "2019-09-01T12:00-0500",
        "company": hstern,
        "likes": 0,
        "product": {
            "name": "Anel",
            "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            "price": 800.00,
            "url": "https://www.hstern.com.br/",
            "blobs": [
                {
                    "type": "image",
                    "file": IMAGES_SERVER_URL + "jewelry0.jpeg"
                },
                {
                    "type": "image",
                    "file": IMAGES_SERVER_URL + "jewelry1.jpeg"
                }
            ]
        }
    },

    {
        "datetime": "2019-08-01T12:00-0500",
        "company": hstern,
        "likes": 0,
        "product": {
            "name": "Conjunto Brinco e Anel",
            "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            "price": 2500.00,
            "url": "https://www.hstern.com.br/",
            "blobs": [
                {
                    "type": "image",
                    "file": IMAGES_SERVER_URL + "jewelry0.jpeg"
                }
            ]
        }
    },

    {
        "datetime": "2019-07-01T12:00-0500",
        "company": boticario,
        "likes": 0,
        "product": {
            "name": "Maquiagem",
            "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            "price": 260.00,
            "url": "https://www.boticario.com.br/",
            "blobs": [
                {
                    "id": "blob9",
                    "type": "image",
                    "file": IMAGES_SERVER_URL + "makeup0.jpg"
                },
                {
                    "id": "blob10",
                    "type": "image",
                    "file": IMAGES_SERVER_URL + "makeup1.jpg"
                },
                {
                    "id": "blob11",
                    "type": "image",
                    "file": IMAGES_SERVER_URL + "makeup2.jpg"
                }
            ]
        }
    },

    {
        "datetime": "2019-06-01T12:00-0500",
        "company": arezzo,
        "likes": 0,
        "product": {
            "name": "Tênis Feminino",
            "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            "price": 450.00,
            "url": "https://www.arezzo.com.br/",
            "blobs": [
                {
                    "type": "image",
                    "file": IMAGES_SERVER_URL + "shoes0.jpg"
                },
                {
                    "type": "image",
                    "file": IMAGES_SERVER_URL + "shoes1.jpg"
                },
                {
                    "type": "image",
                    "file": IMAGES_SERVER_URL + "shoes2.jpg"
                }
            ]
        }
    },

    {
        "datetime": "2019-05-01T12:00-0500",
        "company": daffiti,
        "likes": 0,
        "product": {
            "name": "Tênis Feminino",
            "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            "price": 420.00,
            "url": "https://www.dafiti.com.br/",
            "blobs": [
                {
                    "type": "image",
                    "file": IMAGES_SERVER_URL + "shoes0.jpg"
                },
                {
                    "type": "image",
                    "file": IMAGES_SERVER_URL + "shoes1.jpg"
                },
                {
                    "type": "image",
                    "file": IMAGES_SERVER_URL + "shoes2.jpg"
                }
            ]
        }
    }
]

users = db.users
feeds = db.feeds
likes = db.likes
comments = db.comments

user = {"name": "melhores marcas", "account": "@", "signer": "melhores marcas", "authorized": True}
userId = users.insert_one(user).inserted_id

for feed in FEEDS:
    feedId = feeds.insert_one(feed).inserted_id

    nlikes = random.randint(0, 1000)
    for n in range(nlikes):
        likes.insert_one({"feed": feedId, "user": userId})

    ncomments = random.randint(0, 30)
    for n in range(ncomments):
        comments.insert_one(
            {
                "feed": feedId,
                "user": {"userId": userId, "name": "melhores marcas"},
                "datetime": "2019-01-01T12:00-0500",
                "content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            })

db.users.create_index('account')
db.feeds.create_index('company')
db.likes.create_index('feed')
db.likes.create_index([('feed', ASCENDING), ('user', ASCENDING)])
db.comments.create_index('feed')
