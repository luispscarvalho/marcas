@echo off

del .\.env /F/Q
del .\app\environment.json /F/Q

del .\services\feed\URLS.py /F/Q
del .\services\like\URLS.py /F/Q
del .\services\comment\URLS.py /F/Q
del .\services\user\URLS.py /F/Q
del .\database\URLS.py /F/Q

rem FOR /F "tokens=4 delims= " %%i in ('route print ^| find " 0.0.0.0"') do SET HOST_IP=%%i
SET HOST_IP=127.0.0.1
SET ENV_VAR=HOST_IP=%HOST_IP%
echo HostIP: %HOST_IP%

SET API_VERSION="1.0"
SET ENV_JSON={"API_VERSION": %API_VERSION%, "HOST_IP": "%HOST_IP%"}

SET IMAGE_SERVER_URL=IMAGES_SERVER_URL="http://%HOST_IP%:9106"
SET VIDEO_SERVER_URL=VIDEOS_SERVER_URL="http://%HOST_IP%:9106"

echo %ENV_VAR% >> .\.env
echo %ENV_JSON% >> .\app\environment.json

echo DATABASE_HOST="%HOST_IP%" >> .\database\URLS.py
echo DATABASE_PORT=9105 >> .\database\URLS.py
echo %IMAGE_SERVER_URL% >> .\database\URLS.py
echo %VIDEO_SERVER_URL% >> .\database\URLS.py

SET DB_IP=DATABASE_HOST="172.28.1.5"
SET DB_PORT=DATABASE_PORT=27017

echo %DB_IP% >> .\services\feed\URLS.py
echo %DB_PORT% >> .\services\feed\URLS.py
echo %IMAGE_SERVER_URL% >> .\services\feed\URLS.py
echo %VIDEO_SERVER_URL% >> .\services\feed\URLS.py

echo %DB_IP% >> .\services\like\URLS.py
echo %DB_PORT% >> .\services\like\URLS.py
echo %IMAGE_SERVER_URL% >> .\services\like\URLS.py
echo %VIDEO_SERVER_URL% >> .\services\like\URLS.py

echo %DB_IP% >> .\services\comment\URLS.py
echo %DB_PORT% >> .\services\comment\URLS.py
echo %IMAGE_SERVER_URL% >> .\services\comment\URLS.py
echo %VIDEO_SERVER_URL% >> .\services\comment\URLS.py

echo %DB_IP% >> .\services\user\URLS.py
echo %DB_PORT% >> .\services\user\URLS.py
echo %IMAGE_SERVER_URL% >> .\services\user\URLS.py
echo %VIDEO_SERVER_URL% >> .\services\user\URLS.py

copy .\services\utils.py .\services\feed /Y
copy .\services\utils.py .\services\like /Y
copy .\services\utils.py .\services\comment /Y
copy .\services\utils.py .\services\user /Y

copy .\privatekey .\services\feed /Y
copy .\privatekey .\services\like /Y
copy .\privatekey .\services\comment /Y
copy .\privatekey .\services\user /Y