Feature: Adicionando e removendo comentarios

    Scenario Outline: O usuario deseja comentar sobre um produto
        Given que a tela de comentarios esteja sendo exibida
        When o usuario clica sobre o botao de adicionar comentario e digita o <comentario>
        Then o <comentario> do usuario aparece na lista de comentarios

    Examples: Comentarios
        | comentario                                                  |
        | comentario curto                                            |
        | comentario maior do que o primeiro, sendo, portanto, longo  |

    Scenario: O usuario deseja remover seus comentarios
        Given que a tela de comentarios contenha comentarios do usuario
        When o usuario arrasta os 2 comentarios para a direita e clica sobre o icone de remover
        Then os comentarios do usuario desaparecem da lista
