from appium import webdriver
from time import sleep

def before_all(context):
    caps = {}
    caps['platformName'] = 'Android'
    caps['deviceName'] = '42008b3601c8455d'
    caps['appPackage'] = 'com.melhoresmarcas'
    caps['appActivity'] = '.MainActivity'
    caps['noReset'] = True
    context.driver = webdriver.Remote('http://localhost:4723/wd/hub', caps)

def after_all(context):
    sleep(5)

    context.driver.quit()