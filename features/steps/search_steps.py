from behave import given, when, then, step
from time import sleep


@given('que a tela de pesquisa esteja sendo exibida')
def given(context):
    sleep(10)


@when('o usuario informa {string} no campo de busca')
def when(context, string):
    searchInput = context.driver.find_element_by_accessibility_id(
        "pesquisar por")
    searchInput.click()
    sleep(2)

    searchInput.send_keys(string)
    searchButton = context.driver.find_element_by_accessibility_id('pesquisar')
    searchButton.click()
    sleep(2)


@then('aparecera pelo menos um produto com nome contendo {string}')
def then(context, string):
    product = context.driver.find_element_by_accessibility_id(
        'produto Maquiagem 5e3a2dd2f633071cd9cd066a')

    assert string in product.text
