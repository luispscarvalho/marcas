from behave import given, when, then, step
from time import sleep


def getNumberOfLikes(driver):
    likes = driver.find_element_by_accessibility_id("likes")
    numberOfLikes = int(likes.text)

    return numberOfLikes


@given('que a tela de detalhes do produto esteja sendo exibida')
def given(context):
    product = context.driver.find_element_by_accessibility_id(
        "feed 5e3a2dd2f633071cd9cd066a")
    product.click()
    product.click()
    sleep(2)

    context.likes = getNumberOfLikes(context.driver)


@when('o usuario clica sobre o botao de like')
def when1(context):
    likeButton = context.driver.find_element_by_accessibility_id("gostar")
    likeButton.click()
    sleep(2)

    context.oneMoreLike = getNumberOfLikes(context.driver)


@then('aumenta o numero de likes do produto')
def then1(context):
    assert context.oneMoreLike > context.likes


@when('o usuario clica sobre o botao de dislike')
def when2(context):
    dislikeButton = context.driver.find_element_by_accessibility_id(
        "remover gostar")
    dislikeButton.click()
    sleep(2)

    context.oneLessLike = getNumberOfLikes(context.driver)


@then('diminui o numero de likes do produto')
def then2(context):
    assert context.oneLessLike == context.likes
