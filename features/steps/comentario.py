from appium.webdriver.common.touch_action import TouchAction
from behave import given, when, then, step
from time import sleep


@given('que a tela de comentarios esteja sendo exibida')
def given1(context):
    try:
        comments = context.driver.find_element_by_accessibility_id(
            "comentarios")
        comments.click()
        sleep(2)
    except:
        pass


@when('o usuario clica sobre o botao de adicionar comentario e digita o {comentario}')
def when1(context, comentario):
    addComment = context.driver.find_element_by_accessibility_id(
        "novo comentario")
    addComment.click()
    sleep(2)

    comment = context.driver.find_element_by_accessibility_id("comentario")
    comment.send_keys(comentario)

    send = context.driver.find_element_by_accessibility_id("enviar comentario")
    send.click()
    sleep(2)


@then('o {comentario} do usuario aparece na lista de comentarios')
def then1(context, comentario):
    comment = context.driver.find_element_by_xpath(
        "(//android.widget.TextView[@content-desc=\"comentario na lista\"])[1]")
    assert comentario == comment.text


@given('que a tela de comentarios contenha comentarios do usuario')
def given2(context):
    pass


@when('o usuario arrasta os {n} comentarios para a direita e clica sobre o icone de remover')
def when2(context, n):
    ncomments = int(n)
    for c in range(ncomments):
        TouchAction(context.driver).press(x=974, y=339).wait(
            ms=1000).move_to(x=426, y=328).release().perform()
        sleep(2)

        removeComment = context.driver.find_element_by_accessibility_id("remover")
        removeComment.click()
        sleep(2)

        confirm = context.driver.find_element_by_id("android:id/button1")
        confirm.click()
        sleep(2)

        TouchAction(context.driver).press(x=486, y=303).move_to(x=465, y=900).release().perform()
        sleep(4)


@then('os comentarios do usuario desaparecem da lista')
def then2(context):
    autor = context.driver.find_element_by_xpath(
        "(//android.widget.TextView[@content-desc=\"autor\"])[1]")
    assert "Você:" != autor.text
