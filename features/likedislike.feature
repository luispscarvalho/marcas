Feature: Gostar do produto

    Scenario: O usuario deseja registrar que gostou do produto
        Given que a tela de detalhes do produto esteja sendo exibida
        When o usuario clica sobre o botao de like
        Then aumenta o numero de likes do produto
        When o usuario clica sobre o botao de dislike
        Then diminui o numero de likes do produto
