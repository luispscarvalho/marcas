Feature: Procurando por um produto

    Scenario: O usuario necessita procurar por um produto
        Given que a tela de pesquisa esteja sendo exibida
        When o usuario informa Maquiagem no campo de busca
        Then aparecera pelo menos um produto com nome contendo Maquiagem
