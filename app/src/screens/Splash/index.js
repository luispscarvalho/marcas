import React from 'react';

import SyncStorage from 'sync-storage';

import {
    SplashContainer,

    Warn
} from './styles';
import {
    Logo,

    CenterOfTheSameColumn,
    VerticalSpacer
} from '../../styles';
import {
    ConfigureGoogleSigner,
    SignOptions,
    IsSignedIn,

    SIGNERS,
    ERRORS
} from '../../components/Signer';
import { authorize, authorizeAnonymous } from '../../API/Users';
import { Action } from '../../components/Action';
import Reload from '../../components/Reload';
import logo from '../../assets/logo.png';

export default class Splash extends React.Component {
    constructor(props) {
        super(props);

        this.signInAsAnonymous = this.signInAsAnonymous.bind(this);
        this.onSignIn = this.onSignIn.bind(this);
        this.refresh = this.refresh.bind(this);

        this.state = {
            showSignInOptions: false,

            error: null
        };
    }

    componentDidMount() {
        // TODO gravar no banco local o tipo de autenticacao escolhida
        this.getSignedUser();
    }

    getAuthorization() {
        authorize().then(response => {
            error = response.error;
            if (error.id) {
                this.setState({ showSignInOptions: false, showReload: true, error: error });
            } else {
                const user = SyncStorage.get('user');
                console.debug('authorized user: ' + JSON.stringify(user))

                this.gotoFeeds();
            }
        }).catch((error) => {
            console.debug("authorize.error: " + JSON.stringify(error));
        });
    }

    getSignedUser() {
        ConfigureGoogleSigner();

        IsSignedIn(SIGNERS.Google).then((user) => {
            SyncStorage.set('user', user);

            this.getAuthorization();
        }).catch((error) => {
            if (error === ERRORS.NO_SIGNED_USER) {
                // TODO try to sign facebook user
                this.setState({
                    showSignInOptions: true, showReload: false
                });
            } else {
                console.debug(error);
            }
            // dismiss other errors, user should try to sign in using the sign-in buttons
        });
    }

    onSignIn(user) {
        SyncStorage.set('user', user);

        this.getAuthorization();
    }

    gotoFeeds() {
        const { navigate } = this.props.navigation;
        navigate('Feeds');
    }

    refresh() {
        const user = SyncStorage.get('user');
        if (user) {
            this.getAuthorization();
        } else {
            this.setState({ showSignInOptions: true, showReload: false });
        }
    }

    signInAsAnonymous() {
        authorizeAnonymous();

        this.gotoFeeds();
    }

    showSignInOptions() {
        return (
            <SplashContainer>
                <CenterOfTheSameColumn>
                    <Logo source={logo} />
                    <SignOptions user={null} onSignIn={this.onSignIn} />
                    <VerticalSpacer />
                    <VerticalSpacer />
                    <Warn>Clicando no botão abaixo, você poderá</Warn>
                    <Warn>acessar a aplicação sem logar, mas</Warn>
                    <Warn>algumas opções estarão desabilitadas</Warn>
                    <VerticalSpacer />
                    <Action icon={"login"} title=" Acessar sem logar"
                        onPress={() => { this.signInAsAnonymous() }} />
                </CenterOfTheSameColumn>
            </SplashContainer>
        );
    }

    render() {
        const { showSignInOptions, error } = this.state;

        if (showSignInOptions) {
            return this.showSignInOptions();
        } else if (error) {
            return (<Reload error={error} onReload={this.refresh} />);
        } else {
            return null;
        }
    }

}