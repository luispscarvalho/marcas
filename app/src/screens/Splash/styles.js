import styled from 'styled-components/native';

export const SplashContainer = styled.ScrollView`
    flexDirection: column;
    width: 100%;
    height: 100%;
    background-color: #2b5eA3;
`;

export const Warn = styled.Text`
    padding: 2px;
    font-size: 14;
    color: #f3f3f3;
`;