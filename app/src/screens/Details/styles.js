import styled from 'styled-components/native';

export const DetailsContainer = styled.ScrollView`
    flexDirection: column;
    width: 100%;
    height: 100%;
    background-color: #3F6EA3;
`;

export const CompanyName = styled.Text`
    padding: 8px;
    font-size: 16;
    color: #f3f3f3;
`;

export const CompanyIconSet = styled.View`
    marginVertical: 15;
    marginHorizontal: 2;
`;

export const ProductName = styled.Text`
    padding: 10px;
    color: #59594a;
    font-size: 20;
    font-weight: bold;
`;

export const ProductDescription = styled.Text`
    padding: 10px;
    color: #59594a;
    font-size: 14;
`;

export const ProductPrice = styled.Text`
    color: #59594a;
    font-size: 15;
`;

export const Like = styled.Text`
    color: #59594a;
    font-size: 15;
`;

export const SliderStyle = {
    DotColor: '#ffad05',
    InactiveDotColor: '#5995ed',
    DotWidth: 15,
    DotHeight: 15,
    
    BorderRadius: 15,
    MarginHorizontal: 5
};