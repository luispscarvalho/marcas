import React from 'react';
import { Alert } from 'react-native';
import { Header } from 'react-native-elements';

import { SliderBox } from 'react-native-image-slider-box';
import Icon from 'react-native-vector-icons/AntDesign';
import { Grid, Col } from "react-native-easy-grid";
import Toast from 'react-native-simple-toast';
import Reload from '../../components/Reload';
import CardView from 'react-native-cardview';
import SyncStorage from 'sync-storage';

import {
    Avatar,

    LeftOfTheSameRow,
    RightOfTheSameRow,
    VerticalSpacer,
    HorizontalSpacer,
    Divider,
    Padder,
    CenterOfTheSameRow,

    HeaderStyle,
    LikeStyle
} from '../../styles';
import {
    DetailsContainer,
    CompanyName,
    CompanyIconSet,

    ProductName,
    ProductDescription,
    ProductPrice,

    Like,

    SliderStyle
} from './styles';
import { Action, ACTION_TYPE } from '../../components/Action';
import Sharable from '../../components/Sharable';

import { isAlive, countLikes, like, dislike, likedBy } from '../../API/Likes';
import { find } from '../../API/Feeds';

export default class Details extends React.Component {

    constructor(props) {
        super(props);

        this.like = this.like.bind(this);
        this.dislike = this.dislike.bind(this);
        this.refreshAfterLikeDislikeError = this.refreshAfterLikeDislikeError.bind(this);

        this.loadFeed = this.loadFeed.bind(this);

        this.state = {
            feedId: this.props.navigation.state.params.feedId,
            feed: null,

            likes: 0,
            likeId: null,

            error: null,
            likeDislikeError: null
        };
    }

    componentDidMount() {
        this.loadFeed();
    }

    loadFeed() {
        const { feedId, likeDislikeError } = this.state;

        if (likeDislikeError) {
            return;
        }

        find(feedId).then(response => {
            const error = response.error;

            if (error.id) {
                this.setState({ error: error });
            } else {
                const feed = response.feed;

                isAlive().then((response) => {
                    if (response.isAlive === 'yes') {
                        this.loadLikes(feed);
                    } else {
                        this.setState({
                            feed: feed,

                            likes: 0,
                            likeId: null,

                            error: null
                        });
                    }
                })
            }
        });
    }

    loadLikes(feed) {
        const user = SyncStorage.get('user');

        countLikes(feed._id.$oid).then((likes) => {
            if (!user.anonymous) {
                likedBy(feed._id.$oid).then((likedBy) => {
                    this.setState({
                        feed: feed,

                        likes: likes.count,
                        likeId: likedBy.likeId,

                        error: null
                    });
                });
            } else {
                this.setState({
                    feed: feed,

                    likes: likes.count,
                    likeId: null,

                    error: null
                });
            }
        });
    }

    refreshAfterLikeDislikeError() {
        this.setState({ likeDislikeError: null }, () => {
            this.loadFeed();
        });
    }

    like() {
        const { feedId, feed } = this.state;

        like(feedId).then((like) => {
            if (like.error.id) {
                this.setState({ likeDislikeError: like.error });
            } else {
                this.loadLikes(feed);

                Toast.show('Obrigado pela sua avaliação', Toast.LONG);
            }
        });
    }

    dislike() {
        const { likeId, feed } = this.state;

        dislike(likeId).then((dislike) => {
            if (dislike.error.id) {
                this.setState({ likeDislikeError: dislike.error });
            } else {
                this.loadLikes(feed);

                Toast.show('Avaliação removida com sucesso', Toast.LONG);
            }
        });
    }

    showSlides(product) {
        let slides = []
        product.blobs.map((blob) => {
            slides = [...slides, blob.file];
        });

        return (
            <SliderBox
                dotColor={SliderStyle.DotColor}
                inactiveDotColor={SliderStyle.InactiveDotColor}

                resizeMethod={'resize'}
                resizeMode={'cover'}

                dotStyle={{
                    width: SliderStyle.DotWidth,
                    height: SliderStyle.DotHeight,
                    borderRadius: SliderStyle.BorderRadius,
                    marginHorizontal: SliderStyle.MarginHorizontal,
                    padding: 0,
                    margin: 0
                }}

                images={slides} />
        );
    }

    showDetails() {
        const { navigate } = this.props.navigation;
        const { feed, feedId, likes } = this.state;
        const user = SyncStorage.get('user');

        return (<>
            <Grid>
                <Col size={1} />
                <Col size={98}>
                    <CardView
                        cardElevation={2}
                        cardMaxElevation={2}
                        cornerRadius={0}>
                        <CenterOfTheSameRow>
                            {this.showSlides(feed.product)}
                        </CenterOfTheSameRow>
                        <CenterOfTheSameRow>
                            <ProductName>{feed.product.name}</ProductName>
                        </CenterOfTheSameRow>
                        <Divider />
                        <ProductDescription>{feed.product.description}</ProductDescription>
                        <Padder>
                            <LeftOfTheSameRow>
                                <ProductPrice>{"R$ " + feed.product.price}</ProductPrice>
                                <HorizontalSpacer />
                                {<>
                                    <Icon color={LikeStyle.Color} size={LikeStyle.Size} name="heart" />
                                    <Like accessibilityLabel={"likes"}> {likes}</Like>
                                </>}
                            </LeftOfTheSameRow>
                        </Padder>
                        <Divider />
                        <VerticalSpacer />
                        <RightOfTheSameRow>
                            {!user.anonymous && <Action accessibilityLabel={"comentarios"} icon={"message1"} title=" Comentários"
                                onPress={() => navigate('Comments', { feedId: feedId, company: feed.company, product: feed.product })} />}
                            <HorizontalSpacer />
                            <Action icon={"shoppingcart"}
                                title=" Comprar"
                                onPress={() => {
                                    Alert.alert(
                                        null, 'Ainda não implementado')
                                }} />
                            <HorizontalSpacer />
                        </RightOfTheSameRow>
                        <VerticalSpacer />
                    </CardView>
                </Col>
                <Col size={1} />
            </Grid>
        </>);
    }

    showDetailsLayout() {
        const { feed, likeId } = this.state;
        const user = SyncStorage.get('user');

        return (
            <>
                <Header
                    leftComponent={
                        <Action icon={'left'} type={ACTION_TYPE.ICONIC}
                            onPress={() => { this.props.navigation.goBack() }} />}
                    centerComponent={
                        <Padder>
                            <Avatar source={{ uri: feed.company.avatar }} />
                            <CompanyName>{feed.company.name}</CompanyName>
                        </Padder>
                    }
                    rightComponent={
                        <CompanyIconSet>
                            <RightOfTheSameRow>
                                <Sharable item={feed.product} />
                                {!user.anonymous &&
                                    <>
                                        <HorizontalSpacer />
                                        <HorizontalSpacer />
                                        {(likeId) && <Action icon="heart" accessibilityLabel={"remover gostar"} iconColor={LikeStyle.LikedByUserColor} type={ACTION_TYPE.ICONIC}
                                            onPress={() => this.dislike()} />}
                                        {(!likeId) && <Action icon="hearto" accessibilityLabel={"gostar"} type={ACTION_TYPE.ICONIC}
                                            onPress={() => this.like()} />}
                                    </>
                                }
                            </RightOfTheSameRow>
                        </CompanyIconSet>
                    }

                    containerStyle={HeaderStyle} />
                <DetailsContainer>
                    {this.showDetails()}
                </DetailsContainer>
            </>
        );
    }

    render() {
        const { error, likeDislikeError, feed } = this.state;
        if (likeDislikeError) {
            return (<Reload error={likeDislikeError} onReload={this.refreshAfterLikeDislikeError} />);
        }

        if (error) {
            return (<Reload error={error} onReload={this.loadFeed} />);
        }

        if (feed) {
            return (this.showDetailsLayout());
        }

        return null;
    }

}
