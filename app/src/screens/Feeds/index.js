import React from 'react';
import { View, FlatList, Alert } from 'react-native';
import { Header } from 'react-native-elements';

import DrawerLayout from 'react-native-drawer-layout';
import Toast from 'react-native-simple-toast';
import SyncStorage from 'sync-storage';

import {
    Loading,

    CenterOfTheSameRow,
    HorizontalSpacer,

    HeaderStyle,
    LikeStyle
} from "../../styles";
import {
    SearchInput,
    FeedsContainer
} from "./styles";
import { Action, ACTION_TYPE } from '../../components/Action';
import FeedCard from '../../components/FeedCard';
import Reload from '../../components/Reload';
import Menu from '../../components/Menu';
import { load } from '../../API/Feeds';

export default class Feeds extends React.Component {

    constructor(props) {
        super(props);

        this.toggleMenu = this.toggleMenu.bind(this);
        this.onMenuOpen = this.onMenuOpen.bind(this);
        this.onMenuClose = this.onMenuClose.bind(this);

        this.gotoDetails = this.gotoDetails.bind(this);
        this.forceRefresh = this.forceRefresh.bind(this);

        this.showSearchBar = this.showSearchBar.bind(this);
        this.clearsearchInput = this.clearsearchInput.bind(this);
        this.updateSearchInput = this.updateSearchInput.bind(this);
        this.filterBySearchInput = this.filterBySearchInput.bind(this);

        this.showAll = this.showAll.bind(this);
        this.getLikedByUser = this.getLikedByUser.bind(this);
        this.showLikedByUser = this.showLikedByUser.bind(this);

        this.state = {
            error: null,
            loading: false,
            refreshing: false,

            menuOpened: false,

            searchInput: null,
            filterBySearchInput: false,
            likedByUser: false,

            endOfFeeds: false,
            nextPage: 0,
            feeds: []
        };
    }

    componentDidMount() {
        if (this.willRefresh) {
            this.willRefresh.remove();
        }

        console.log("loading first feeds...");
        this.loadFeeds();
    }

    loadFeeds() {
        const { loading, nextPage, feeds } = this.state;
        if (loading) {
            return;
        }

        this.setState({ loading: true }, () => {
            load(nextPage, this.getSearchFor(), this.getSelectedCompanies(), this.getLikedByUser()).
                then(response => {
                    const error = response.error;
                    if (error.id) {
                        this.setState({ error: error, loading: false, refreshing: false });
                    } else {
                        const moreFeeds = response.feeds;

                        if (moreFeeds.length) {
                            SyncStorage.set('companies', response.companies);

                            this.setState({
                                loading: false,
                                refreshing: false,
                                error: null,

                                endOfFeeds: false,
                                nextPage: nextPage + 1,
                                feeds: [...feeds, ...moreFeeds]
                            });
                        } else {
                            this.setState({
                                loading: false,
                                refreshing: false,
                                error: null,

                                endOfFeeds: true
                            }, () => {
                                if (!feeds.length) {
                                    Toast.show('Não há produtos para exibir', Toast.LONG);
                                }
                            });

                        }
                    }
                }).catch(error =>
                    this.setState({ error: error, loading: false, refreshing: false })
                )
        });
    }

    loadNextFeeds() {
        const { loading, endOfFeeds } = this.state;
        if (loading || endOfFeeds) {
            return;
        }

        console.log("loading next feeds now...");
        this.loadFeeds();
    }

    refresh() {
        const { refreshing } = this.state;
        if (refreshing) {
            return;
        }

        this.setState({ refreshing: true, nextPage: 0, feeds: [] }, () => {
            console.log("refreshing feeds now...");
            this.loadFeeds();
        });
    }

    forceRefresh() {
        this.setState({ refreshing: false }, () => {
            this.refresh();
        });
    }

    gotoDetails(feed) {
        const { navigate } = this.props.navigation;

        this.willRefresh = this.props.navigation.addListener(
            'willFocus',
            () => {
                this.forceRefresh();
            }
        );
        navigate('Details', { feedId: feed._id.$oid });
    }

    getSearchFor() {
        const { filterBySearchInput, searchInput } = this.state;

        let searchFor = { product: "" };
        if (filterBySearchInput) {
            searchFor = { product: searchInput };
        }

        return searchFor;
    }

    getSelectedCompanies() {
        let oids = { oids: [] };

        const selecteds = SyncStorage.get('selectedCompanies');
        if (selecteds) {
            oids = { oids: selecteds };
        }

        return oids;
    }

    getLikedByUser() {
        const { likedByUser } = this.state;

        return (likedByUser ? '1' : '0');
    }

    onMenuOpen() {
        this.setState({
            menuOpened: true,
        });
    }

    onMenuClose() {
        this.setState({
            menuOpened: false, likedByUser: false, searchInput: null,
            filterBySearchInput: false
        }, () => {
            this.refresh();
        });
    }

    toggleMenu() {
        if (this.state.menuOpened) {
            this.menu.closeDrawer();
        } else {
            this.menu.openDrawer();
        }
    }

    updateSearchInput(searchInput) {
        this.setState({ searchInput: searchInput });
    }

    filterBySearchInput() {
        const { searchInput } = this.state;
        if ((searchInput === null) || (searchInput === "")) {
            Alert.alert(null, 'Digite algo para pesquisar');

            return;
        }

        this.setState({ filterBySearchInput: true }, () => {
            this.refresh();
        });
    }

    clearsearchInput() {
        this.setState({ searchInput: null, filterBySearchInput: false }, () => {
            this.refresh();
        });
    }

    showLikedByUser() {
        this.setState({ likedByUser: true }, () => {
            this.forceRefresh();
        });
    }

    showAll() {
        this.setState({ likedByUser: false }, () => {
            this.forceRefresh();

        });
    }

    showFeeds() {
        const { feeds, refreshing, loading } = this.state;

        return (
            <FlatList
                data={feeds}

                numColumns={2}

                ListFooterComponent={loading && <Loading />}

                onEndReached={() => this.loadNextFeeds()}
                onEndReachedThreshold={0.1}

                onRefresh={() => this.refresh()}
                refreshing={refreshing}

                keyExtractor={(item) => String(item._id.$oid)}
                renderItem={({ item }) => {
                    return (
                        <View style={{ width: '50%' }}>
                            <FeedCard feed={item} gotoDetails={this.gotoDetails} />
                        </View>
                    )
                }}
            />
        )
    }

    showSearchBar() {
        const { searchInput, filterBySearchInput } = this.state;

        return (
            <CenterOfTheSameRow>
                <SearchInput
                    accessibilityLabel={'pesquisar por'}
                    autoCompleteType={'name'}
                    onChangeText={(keys) => { this.updateSearchInput(keys) }}
                    value={searchInput} />
                <HorizontalSpacer />
                {!filterBySearchInput &&
                    <Action 
                        accessibilityLabel={'pesquisar'}
                        icon={"search1"} type={ACTION_TYPE.ICONIC}
                        onPress={this.filterBySearchInput} />}
                {filterBySearchInput &&
                    <Action icon="close" type={ACTION_TYPE.ICONIC}
                        accessibility={'clearSearchButton'}
                        onPress={this.clearsearchInput} />}

            </CenterOfTheSameRow>
        );
    }

    showDrawerLayout() {
        const { likedByUser } = this.state;
        const user = SyncStorage.get('user');

        return (
            <DrawerLayout
                drawerWidth={250}

                ref={drawerElement => {
                    this.menu = drawerElement;
                }}
                drawerPosition={DrawerLayout.positions.left}

                onDrawerOpen={this.onMenuOpen}
                onDrawerClose={this.onMenuClose}

                renderNavigationView={() => <Menu />}>
                <Header
                    leftComponent={
                        <Action icon={'menuunfold'} type={ACTION_TYPE.ICONIC} onPress={this.toggleMenu} />}
                    centerComponent={this.showSearchBar}
                    rightComponent={
                        !user.anonymous && <>
                            {likedByUser && <Action icon={'heart'} iconColor={LikeStyle.LikedByUserColor} type={ACTION_TYPE.ICONIC}
                                onPress={this.showAll} />}
                            {!likedByUser && <Action icon={'hearto'} type={ACTION_TYPE.ICONIC}
                                onPress={this.showLikedByUser} />}
                        </>}

                    containerStyle={HeaderStyle}
                />
                <FeedsContainer>
                    {this.showFeeds()}
                </FeedsContainer>
            </DrawerLayout>
        )
    }

    render() {
        const { error } = this.state;

        if (error) {
            return (<Reload error={error} onReload={this.forceRefresh} />);
        } else {
            return this.showDrawerLayout();
        }
    }
}