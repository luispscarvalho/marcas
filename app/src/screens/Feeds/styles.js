import styled from 'styled-components/native';

export const SearchInput = styled.TextInput`
    height: 40px;
    width: 100%;
    background-color: #fff;
    border-color: #DAE4D3;
    border-width: 1;
    border-radius: 8px;
`;

export const OrderBy = styled.TouchableOpacity`
    flexDirection: row;
    justify-content: center;
    align-items: center;
`;

export const FeedsContainer = styled.View`
    flex: 1;
    flexDirection: row;

    width: 100%;
    height: 100%;
    background-color: #3F6EA3;
`;