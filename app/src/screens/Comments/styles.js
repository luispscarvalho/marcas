import styled from 'styled-components/native';

export const CommentsContainer = styled.View`
    flexDirection: column;
    width: 100%;
    height: 100%;
    background-color: #3F6EA3;
`;

export const UserCommentContainer = styled.View`
    background-color: #78a1bb;
`;

export const OtherUserCommentContainer = styled.View`
    background-color: #1d84b5;
`;

export const CommentSpacer = styled.View`
    marginVertical: 2;
`;

export const NewCommentContainer = styled.View`
    margin-top: 100;
    align-self: center;
    width: 95%;
    border-color: #2b5eA3;
    border-width: 1;
    border-radius: 6;
    background-color: #f6f6f6;
`;

export const RemoveCommentAction = styled.View`
    flex: 1;
    justifyContent: center;
    padding-left: 20;
    background-color: #379392;
`;

export const ProductName = styled.Text`
    padding: 8px;
    font-size: 16;
    color: #f3f3f3;
`;

export const Author = styled.Text`
    padding: 6px;
    font-size: 16;
    color: #283044;
`;

export const Comment = styled.Text`
    padding: 6px;
    font-size: 16;
    color: #283044;
`;

export const CommentDate = styled.Text`
    padding: 6px;
    font-size: 14;
    color: #283044;
`;

export const CommentStyle = {
    RemoveCommentIconColor: '#f3fef3', 

    OthersCommentsBackgroundColor: '#d6eaf8', 
    UserCommentsBackgroundColor: '#e5f2c9'
};
