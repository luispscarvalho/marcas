import React from 'react';
import { Modal, Text, TextInput, FlatList, Alert } from 'react-native';
import { Header } from 'react-native-elements';

import Swipeable from 'react-native-swipeable-row';
import Toast from 'react-native-simple-toast';
import SyncStorage from 'sync-storage';
import Moment from 'react-moment';

import 'moment-timezone';

import {
    Avatar,
    Loading,

    Padder,
    Divider,
    RightOfTheSameRow,
    HorizontalSpacer,
    VerticalSpacer,

    HeaderStyle
} from '../../styles';
import {
    Author,
    ProductName,

    CommentsContainer,
    NewCommentContainer,
    CommentSpacer,
    Comment,
    CommentDate,
    RemoveCommentAction,
    UserCommentContainer,
    OtherUserCommentContainer
} from './styles';
import { Action, ACTION_TYPE } from '../../components/Action';
import { load, add, remove } from '../../API/Comments';
import Reload from '../../components/Reload';

const MAX_COMMENT_LENGTH = 100;

export default class Comments extends React.Component {

    constructor(props) {
        super(props);

        this.onReload = this.onReload.bind(this);
        this.confirmRemoval = this.confirmRemoval.bind(this);

        this.state = {
            feedId: this.props.navigation.state.params.feedId,
            company: this.props.navigation.state.params.company,
            product: this.props.navigation.state.params.product,

            addCommentVisible: false,

            isAlive: true,
            nextPage: 0,
            loading: false,
            refreshing: false,

            comments: [],
            newComment: '',
            endOfComments: false,

            error: null
        }
    }

    componentDidMount() {
        this.loadComments();
    }

    loadComments() {
        const { loading } = this.state;
        if (loading) {
            return;
        }

        const { feedId, nextPage } = this.state;

        this.setState({ loading: true });

        load(nextPage, feedId).then((data) => {
            if (data.error.id) {
                this.setState({ error: data.error });
            } else {
                const moreComments = data.comments;

                if (moreComments.length) {
                    const { comments } = this.state;

                    this.setState({
                        nextPage: nextPage + 1,
                        loading: false,
                        refreshing: false,
                        error: null,

                        endOfComments: false,
                        comments: [...comments, ...moreComments]
                    });
                } else {
                    this.setState({
                        loading: false,
                        refreshing: false,
                        error: null,

                        endOfComments: true
                    });
                }
            }
        });

    }

    loadNextComments() {
        const { loading, endOfComments } = this.state;
        if (loading || endOfComments) {
            return;
        }

        console.log("loading next comments...");
        this.loadComments();
    }

    refresh() {
        const { refreshing } = this.state;
        if (refreshing) {
            return;
        }

        this.setState({ refreshing: true, loading: false, nextPage: 0, comments: [] },
            () => {
                console.log("refreshing comments now...");
                this.loadComments();
            });
    }

    keep(comment) {
        this.setState({ newComment: comment });
    }

    toggleAddComment() {
        const { addCommentVisible } = this.state;
        this.setState({ addCommentVisible: !addCommentVisible });
    }

    addComment() {
        const { feedId, newComment } = this.state;

        add(feedId, newComment).then((response) => {
            const error = response.error

            if (error.code) {
                this.setState({ error: error });
            } else {
                this.toggleAddComment();
                this.refresh();
            }
        });
    }

    remove(comment) {
        remove(comment._id.$oid).then((response) => {
            if (response.result === 'ok') {
                Toast.show('Comentário removido com sucesso', Toast.LONG);
            }

            this.refresh();
        });
    }

    confirmRemoval(comment) {
        Alert.alert(
            null,
            'Remover este comentário?',
            [
                { text: 'NÃO', style: 'cancel' },
                { text: 'SIM', onPress: () => this.remove(comment) },
            ]
        );
    }

    onReload() {
        this.setState({ refreshing: false }, () => {
            this.refresh();
        });
    }

    showOtherUserComment(comment) {
        return (
            <>
                <OtherUserCommentContainer>
                    <Author accessibilityLabel={"autor"}>{comment.user.name}:</Author>
                    <Comment accessibilityLabel={"comentario na lista"}>{comment.content}</Comment>
                    <CommentDate accessibilityLabel={"data do comentario"}>
                        <Moment element={Text} parse="YYYY-MM-DD HH:mm" format="DD/MM/YYYY HH:mm">
                            {comment.datetime}
                        </Moment>
                    </CommentDate>
                </OtherUserCommentContainer>
                <CommentSpacer />
            </>
        );
    }

    showUserComment(comment) {
        return (
            <>
                <Swipeable
                    rightButtonWidth={60}
                    rightButtons={[
                        <RemoveCommentAction>
                            <Action accessibilityLabel={"remover"} type={ACTION_TYPE.ICONIC} icon={'delete'} onPress={
                                () => {
                                    this.confirmRemoval(comment);
                                }} />
                        </RemoveCommentAction>
                    ]} >
                    <UserCommentContainer>
                        <Author accessibilityLabel={"autor"}>{"Você:"}</Author>
                        <Comment accessibilityLabel={"comentario na lista"}>{comment.content}</Comment>
                        <CommentDate accessibilityLabel={"data do seu comentario"}>
                            <Moment element={Text} parse="YYYY-MM-DD HH:mm:ss" format="DD/MM/YYYY HH:mm:ss">
                                {comment.datetime}
                            </Moment>
                        </CommentDate>
                    </UserCommentContainer>
                </Swipeable>
                <CommentSpacer />
            </>
        )
    }

    showAddComment() {
        return (
            <Modal
                animationType="slide"
                transparent={true}

                onRequestClose={() => {
                    this.refresh();
                }}>

                <NewCommentContainer>
                    <TextInput
                        accessibilityLabel={"comentario"}
                        multiline
                        onChangeText={(text) => this.keep(text)}
                        editable
                        placeholder={'Digite uma mensagem com até ' + MAX_COMMENT_LENGTH + ' caracteres'}
                        maxLength={MAX_COMMENT_LENGTH} />

                    <Divider />
                    <RightOfTheSameRow>
                        <Action accessibilityLabel={"enviar comentario"} icon={"check"} title=" Enviar"
                            onPress={() => { this.addComment() }} />
                        <HorizontalSpacer />
                        <Action accessibilityLabel={"desistir de comentario"} icon={"closecircle"} title=" Fechar"
                            onPress={() => { this.toggleAddComment() }} />
                        <HorizontalSpacer />
                    </RightOfTheSameRow>
                    <VerticalSpacer />
                </NewCommentContainer>
            </Modal>);
    }

    showCommentsLayout() {
        const { company, product, comments, loading, refreshing, addCommentVisible } = this.state;
        const user = SyncStorage.get('user');

        return (
            <>
                <Header
                    leftComponent={
                        <Action icon={'left'} type={ACTION_TYPE.ICONIC}
                            onPress={() => { this.props.navigation.goBack() }} />}
                    centerComponent={
                        <Padder>
                            <Avatar source={{ uri: company.avatar }} />
                            <ProductName>{product.name}</ProductName>
                        </Padder>
                    }
                    rightComponent={
                        <Action accessibilityLabel={"novo comentario"} icon={'pluscircleo'} type={ACTION_TYPE.ICONIC}
                            onPress={() => { this.toggleAddComment() }} />
                    }

                    containerStyle={HeaderStyle}
                />

                <CommentsContainer>
                    <FlatList
                        data={comments}

                        ListFooterComponent={loading && <Loading />}

                        onEndReached={() => this.loadNextComments()}
                        onEndReachedThreshold={0.1}

                        onRefresh={() => this.refresh()}
                        refreshing={refreshing}

                        keyExtractor={(item) => String(item._id.$oid)}
                        renderItem={({ item }) => {
                            if (item.postedByUser) {
                                return this.showUserComment(item)
                            } else {
                                return this.showOtherUserComment(item)
                            }
                        }}
                    />
                </CommentsContainer>
                {addCommentVisible && this.showAddComment()}
            </>
        )
    }

    render() {
        const { comments, error } = this.state;

        if (error) {
            return (<Reload error={error} onReload={this.onReload} />);
        }

        if (comments && comments.length) {
            return (this.showCommentsLayout());
        } else {
            return null;
        }
    }
}