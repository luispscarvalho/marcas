import SyncStorage from 'sync-storage';

import { fetchFrom } from './Fetcher';

import { API_VERSION } from "../../environment.json";
import { HOST_IP } from "../../environment.json";

const FEED_URL = "http://" + HOST_IP + ":9002/v" + API_VERSION + "/";
const FIND_URL = FEED_URL + "find/";
const DATA_URL = FEED_URL;

export const load = (page, searchFor, companies, likedByUser) => {
    const user = SyncStorage.get('user');

    return fetchFrom(DATA_URL, {
        page: page,

        searchFor: searchFor,
        companies: companies,
        likedByUser: likedByUser,

        userId: encodeURIComponent(user.userId)
    });
}

export const find = (feedId) => {
    return fetchFrom(FIND_URL,
        {
            feedId: feedId
        });
}
