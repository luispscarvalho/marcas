import SyncStorage from 'sync-storage';

import { fetchFrom } from './Fetcher';

import { API_VERSION } from "../../environment.json";
import { HOST_IP } from "../../environment.json";

const COMMENT_URL = "http://" + HOST_IP + ":9003/v" + API_VERSION + "/";
const ADD_URL = COMMENT_URL + "add/";
const REMOVE_URL = COMMENT_URL + "remove/";
const DATA_URL = COMMENT_URL;

export const load = (page, feedId) => {
    const user = SyncStorage.get('user')

    return fetchFrom(DATA_URL,
        {
            page: page,
            feedId: feedId,
            userId: encodeURIComponent(user.userId) 
        });
}

export const add = (feedId, comment) => {
    const user = SyncStorage.get('user')

    return fetchFrom(ADD_URL,
        {
            feedId: feedId,
            userId: encodeURIComponent(user.userId), 
            content: comment
        });
}

export const remove = (commentId) => {
    return fetchFrom(REMOVE_URL, {
        commentId: commentId
    });
}
