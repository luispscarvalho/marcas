import SyncStorage from 'sync-storage';

import { fetchFrom } from './Fetcher';

import { API_VERSION } from "../../environment.json";
import { HOST_IP } from "../../environment.json";

const LIKES_URL = "http://" + HOST_IP + ":9004/v" + API_VERSION + "/";
const IS_ALIVE_URL = LIKES_URL + "isalive";
const COUNT_LIKES_URL = LIKES_URL + "countlikes/"
const LIKE_URL = LIKES_URL + "like/"
const DISLIKE_URL = LIKES_URL + "dislike/"
const LIKED_BY_USER_URL = LIKES_URL + "likedby/";


export const isAlive = () => {
    return fetchFrom(IS_ALIVE_URL, null);
}

export const countLikes = (feedId) => {
    return fetchFrom(COUNT_LIKES_URL,
        {
            feedId: feedId
        });
}

export const like = (feedId) => {
    const user = SyncStorage.get('user');

    return fetchFrom(LIKE_URL,
        {
            feedId: feedId,

            userId: encodeURIComponent(user.userId)
        });
}

export const likedBy = (feedId) => {
    const user = SyncStorage.get('user');

    return fetchFrom(LIKED_BY_USER_URL,
        {
            feedId: feedId,
            
            userId: encodeURIComponent(user.userId)
        });
}

export const dislike = (likeId) => {
    return fetchFrom(DISLIKE_URL,
        {
            likeId: likeId
        });
}

