import { RSA } from 'react-native-rsa-native';

const PUBLIC_KEY = '-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC19wALXE9MRBjxsHopCOJrV50H\nOkumg5Hf0mBQTYaRsnzk+dFkFUcOfAAPdzf0vs/GHEHqYpHC9PXla5p67NSnXWUx\naCOEDwIYmjaYmuu8ng/bdEF4XpFSkLspEyFdYy1WwVxi7aVcluZQS3kJJmGOe8In\njfP67HwSAay7HafvQQIDAQAB\n-----END PUBLIC KEY-----';

export const encrypt = async (encryptable) => {
    let promise = null;

    try {
        let encrypted = await RSA.encrypt(encryptable, PUBLIC_KEY);
        
        promise = Promise.resolve(encrypted);
    } catch (error) {
        console.debug('Erro de encriptacao: ' + error);
        
        promise = Promise.reject(error);
    }

    return promise;
}