import { encrypt } from "./Cypher";

const APP_TOKEN = "On second thought, let's not go to Kamelot. It's a silly place!";

export const fetchFrom = async (baseURL, parameters) => {
    let promise = null;

    const token = await encrypt(APP_TOKEN);

    const headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', token);

    const url = baseURL + (parameters ? encodeURIComponent(JSON.stringify(parameters)) : "");

    console.debug("fetcher.url: " + url);
    try {
        response = await fetch(url, {
            method: 'GET',
            headers: headers
        });
        if (response.ok) {
            promise = Promise.resolve(response.json());
        } else {
            promise = Promise.reject(response);
        }
    } catch (error) {
        promise = Promise.reject(error);
    }

    return promise;
}

