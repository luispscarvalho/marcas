import SyncStorage from 'sync-storage';

import { API_VERSION } from "../../environment.json";
import { HOST_IP } from "../../environment.json";
import { fetchFrom } from './Fetcher';
import { encrypt, decrypt } from './Cypher';

const USER_URL = "http://" + HOST_IP + ":9001/v" + API_VERSION + "/";
const AUTHORIZE_URL = USER_URL + "authorize/";

export const authorize = async () => {
    let promise = null;
    const user = SyncStorage.get('user');

    try {
        const response = await fetchFrom(AUTHORIZE_URL, {
            name: encodeURIComponent(await encrypt(user.name)),
            account: encodeURIComponent(await encrypt(user.account)),
            signer: user.signer
        });
        console.debug('authorize.response: ' + JSON.stringify(response));

        if (!response.error.id) {
            user.userId = response.userId;
            user.anonymous = false;

            SyncStorage.set('user', user);
        }

        promise = Promise.resolve(response);
    } catch (error) {
        promise = Promise.reject(error);
    };

    return promise;
}

export const authorizeAnonymous = () => {
    const user = {
        userId: 'Anonymous_UserId',
        name: 'Anonymous',
        anonymous: true
    }

    SyncStorage.set('user', user);
}