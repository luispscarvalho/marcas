import React from 'react';
import { Button } from 'react-native-elements';

import Icon from 'react-native-vector-icons/AntDesign';

const Style = {
    IconSize: 18,
    IconColor: '#fff'
};

export const ACTION_TYPE = {
    BUTTON: 'button',
    ICONIC: 'iconic'
};

export class Action extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            onPress: this.props.onPress,

            icon: this.props.icon,
            iconColor: this.props.iconColor,

            title: this.props.title,
            type: this.props.type,
            accessibilityLabel: this.props.accessibilityLabel
        }
    }

    render() {
        const { onPress, icon, iconColor, title, type, accessibilityLabel } = this.state;

        if (type === ACTION_TYPE.ICONIC) {
            if (onPress) {
                return (
                    <Icon
                        accessibilityLabel={accessibilityLabel}
                        name={icon}
                        size={Style.IconSize}
                        color={iconColor ? iconColor : Style.IconColor}

                        onPress={() => { onPress() }} />
                );
            } else {
                return (
                    <Icon
                        accessibilityLabel={accessibilityLabel}
                        name={icon}
                        size={Style.IconSize}
                        color={iconColor ? iconColor : Style.IconColor} />
                );
            }
        } else {
            return (
                <Button
                    accessibilityLabel={accessibilityLabel}
                    icon={
                        <Icon
                            name={icon}
                            size={Style.IconSize}
                            color={iconColor ? iconColor : Style.IconColor}
                        />}
                    title={title}
                    type={"solid"}

                    onPress={() => { onPress() }} />
            );
        }
    }
}