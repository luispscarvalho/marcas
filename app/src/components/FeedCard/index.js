import React from 'react';
import { TouchableOpacity } from 'react-native';

import { Card, CardContent, CardImage } from 'react-native-cards';
import Icon from 'react-native-vector-icons/AntDesign';

import {
    Avatar,
    Divider,

    CenterOfTheSameRow,
    LeftOfTheSameRow,
    HorizontalSpacer
} from "../../styles";
import {
    CompanyName,
    ProductName,
    ProductDescription,
    ProductPrice,

    Like,
    LikePadder,

    LikeStyle
} from "./styles";

const SHORT_DESCRIPTION_LIMIT = 50;

export default class FeedCard extends React.Component {
    constructor(props) {
        super(props)

        this.state = ({
            feed: this.props.feed, 
            gotoDetails: this.props.gotoDetails
        });
    }

    shortenDescription(description) {
        let short = "...";

        if (description.length > SHORT_DESCRIPTION_LIMIT) {
            short = description.substring(0, SHORT_DESCRIPTION_LIMIT - short.length) + short;
        }

        return short;
    }

    render() {
        const { feed, gotoDetails } = this.state;
        const image = feed.product.blobs[0].file;

        return (
            <TouchableOpacity 
                accessibilityLabel={"feed " + feed._id.$oid}
                key={feed._id.$oid} onPress={() => gotoDetails(feed)}>
                <Card>
                    <CardImage source={{ uri: image }} />
                    <CardContent>
                        <CenterOfTheSameRow>
                            <Avatar source={{ uri: feed.company.avatar }} />
                            <CompanyName>{feed.company.name}</CompanyName>
                        </CenterOfTheSameRow>
                    </CardContent>
                    <CardContent>
                        <Divider />
                        <ProductName 
                            accessibilityLabel={"produto " + feed.product.name + " " + feed._id.$oid}
                        >{feed.product.name}</ProductName>
                    </CardContent>
                    <CardContent>
                        <ProductDescription>{this.shortenDescription(feed.product.description)}</ProductDescription>
                    </CardContent>
                    <CardContent>
                        <LeftOfTheSameRow>
                            <ProductPrice>{"R$ " + feed.product.price}</ProductPrice>
                            <HorizontalSpacer />
                            <LikePadder>
                                <Icon color={LikeStyle.Color} size={LikeStyle.Size} name="heart">
                                    <Like> {feed.likes}</Like>
                                </Icon>
                            </LikePadder>
                        </LeftOfTheSameRow>
                    </CardContent>
                </Card>
            </TouchableOpacity>);
    }
}