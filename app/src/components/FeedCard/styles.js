import styled from 'styled-components/native';

export const CompanyName = styled.Text`
    padding: 8px;
    font-size: 16;
    color: #59594a;
`;

export const ProductName = styled.Text`
    color: #59594a;
    font-size: 16;
    font-weight: bold;
`;

export const ProductDescription = styled.Text`
    color: #59594a;
    font-size: 14;
`;

export const ProductPrice = styled.Text`
    color: #59594a;
    font-size: 14;
`;

export const Like = styled.Text`
    color: #59594a;
    font-size: 14;
`;

export const LikePadder = styled.View`
    padding: 3px;
`;

export const LikeStyle = {
    Color: '#f7567c',
    Size: 14
};