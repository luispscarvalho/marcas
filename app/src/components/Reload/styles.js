import styled from 'styled-components/native';

export const ReloadContainer = styled.View`
    height: 100%;
    justify-content: center;
    align-items: center;
    background-color: #2b5eA3;
`;

export const Message = styled.Text`
    color: #fff;
`;

