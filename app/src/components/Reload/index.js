import React from 'react';

import {
    Logo,

    VerticalSpacer
} from "../../styles";
import {
    ReloadContainer,
    Message
} from './styles';
import { Action, ACTION_TYPE } from '../../components/Action';
import logo from '../../assets/logo.png';

export default class Reload extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            messages: this.props.messages,
            error: this.props.error,

            onReload: this.props.onReload
        }
    }

    render() {
        let {messages} = this.state;
        const { error, onReload } = this.state;

        if (error) {
            messages = [error.message, "Esse problema pode ser temporário", "Tente novamente daqui a pouco"];
        }

        let idx = 0;
        return (
            <ReloadContainer>
                <Logo source={logo} />
                {messages.map((message) => {
                    return (<Message key={++idx}>{message}</Message>);
                })}
                <VerticalSpacer />
                <Action icon={"reload1"} title=" Recarregar" type={ACTION_TYPE.BUTTON}
                    onPress={() => onReload()} />
            </ReloadContainer >
        );
    }

}