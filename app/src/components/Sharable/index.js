import React from 'react';
import { Share } from 'react-native';

import { Action, ACTION_TYPE } from '../Action';
import { displayName as appName } from '../../../app.json';

export default class Sharable extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            item: this.props.item
        }
    }

    share = async () => {
        const { item } = this.state
        const message = item.url + ' \n\n Enviado por ' + appName + ' \n Baixe agora: https://play.google.com/store'

        try {
            const result = await Share.share({
                title: item.name,
                message: message
            });

            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            alert(error.message);
        }
    };

    render() {
        return (
            <Action icon="sharealt"  type={ACTION_TYPE.ICONIC}
                onPress={() => this.share()} />);
    }
}
