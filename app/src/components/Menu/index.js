import React from 'react';
import { ScrollView, TouchableOpacity, Alert } from 'react-native';

import Icon from 'react-native-vector-icons/AntDesign';
import { Grid, Col } from "react-native-easy-grid";
import Toast from 'react-native-simple-toast';
import SyncStorage from 'sync-storage';

import {
    SignOptions
} from '../../components/Signer';
import {
    Avatar,

    LeftOfTheSameRow,
    Padder,
    HorizontalSpacer,
    VerticalSpacer
} from "../../styles";
import {
    MenuContainer,
    MenuSection,
    MenuItem,
    MenuDivider,
} from "./styles";
import { authorize, authorizeAnonymous } from '../../API/Users';

export default class Menu extends React.Component {

    constructor(props) {
        super(props);

        this.onSignIn = this.onSignIn.bind(this);
        this.onSignOut = this.onSignOut.bind(this);
    }

    checkSelectedCompanies(companies) {
        if (companies) {
            const selecteds = SyncStorage.get('selectedCompanies');
            if (selecteds) {
                companies.map((company) => {
                    company.checked = (selecteds.indexOf(company._id.$oid) > -1);
                });
            }
        }

        return companies;
    }

    saveSelectedCompanies() {
        let selecteds = [];

        const companies = SyncStorage.get('companies');
        companies.map((company) => {
            if (company.checked) {
                selecteds = [...selecteds, company._id.$oid];
            }
        });

        SyncStorage.set('selectedCompanies', selecteds);
    }

    onSignIn(user) {
        SyncStorage.set('user', user);

        authorize().then(response => {
            error = response.error;
            if (error.id) {
                Alert.alert(null, error.message);
            } else {
                const user = SyncStorage.get('user');
                console.debug('authorized user: ' + JSON.stringify(user))

                Toast.show('Conta ' + user.signer + ' acessada com sucesso!', Toast.LONG);

                this.setState({ state: this.state });
            }
        }).catch((error) => {
            console.debug("authorize.error: " + JSON.stringify(error));
        });
    }

    onSignOut(signer) {
        authorizeAnonymous();

        Toast.show('A aplicação saiu de sua conta ' + signer, Toast.LONG);

        this.setState({ state: this.state });
    }

    showCompany(company) {
        return (
            <TouchableOpacity key={company._id.$oid} onPress={() => {
                company.checked = !company.checked;
                this.saveSelectedCompanies();

                this.setState({ state: this.state });
            }}>
                <Padder>
                    <LeftOfTheSameRow>
                        {company.checked ?
                            <Icon style={{ padding: 5 }} size={24} color="#fff" name="check" /> :
                            <Icon style={{ padding: 5 }} size={24} color="#fff" name="plus" />}
                        <HorizontalSpacer />
                        {company.avatar && <Avatar source={{ uri: company.avatar }} />}
                        <MenuItem>{company.name}</MenuItem>
                    </LeftOfTheSameRow>
                </Padder>
                <MenuDivider />
            </TouchableOpacity>
        );
    }

    render() {
        const companies = SyncStorage.get('companies');
        if (!companies) {
            return null;
        }
        this.checkSelectedCompanies(companies);

        return (
            <MenuContainer>
                <ScrollView>
                    <VerticalSpacer />
                    <MenuSection>Filtar por</MenuSection>
                    {companies.map((company) => this.showCompany(company))}
                    <VerticalSpacer />
                    <Grid>
                        <Col size={4} />
                        <Col size={92}>
                            <SignOptions onSignIn={this.onSignIn} onSignOut={this.onSignOut} />
                        </Col>
                        <Col size={4} />
                    </Grid>
                </ScrollView>
            </MenuContainer>
        );
    }
};