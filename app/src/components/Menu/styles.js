import styled from 'styled-components/native';

export const MenuContainer = styled.View`
    flex: 1;
    font-size: 18;
    background-color: #2b5eA3;
`;

export const MenuSection = styled.Text`
    padding: 8px;
    font-size: 16;
    color: #f3f3f3;
`;

export const MenuItem = styled.Text`
    padding: 8px;
    font-size: 14;
    color: #f3f3f3;
`;

export const MenuDivider = styled.View`
    marginVertical: 5;
    marginHorizontal: 5;

    border-bottom-width: 1;
    borderColor: #3F6EA3;
`;
