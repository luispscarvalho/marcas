import React from 'react';
import { View, Alert } from 'react-native';

import { GoogleSignin } from 'react-native-google-signin';

import { Action } from '../../components/Action';
import { VerticalSpacer } from '../../styles';
import SyncStorage from 'sync-storage';

export const SIGNERS = {
    Google: 'Google',
    Facebook: 'Facebook',
    None: 'None'
};

export const ERRORS = {
    NO_SIGNER: 'Falha de configuração do autenticador',
    NO_SIGNED_USER: 'Nenhum usuário encontrado',
    FAILED_TO_SIGNIN: 'Signin falhou',
    FAILED_TO_SIGOUT: 'Signout falhou'
};

const UNRAISABLE_ERRORS = [
    12501
];

export const ConfigureGoogleSigner = () => {
    GoogleSignin.configure({
        webClientId: '705190656531-ifckj99ur8t56nn9uoqui30slsenmtl1.apps.googleusercontent.com',
        offlineAccess: false
    });
}

export const FromGoogleUserToUser = (guser, signer) => {
    return { name: guser.user.name, account: guser.user.email, signer: signer };
}

export const SignIn = async (signer) => {
    if (signer == SIGNERS.Google) {
        try {
            await GoogleSignin.hasPlayServices();
            await GoogleSignin.signIn();

            return Promise.resolve(signer);
        } catch (error) {
            return Promise.reject(error);
        }
    } else {
        return Promise.reject(ERRORS.NO_SIGNER);
    }
}

export const GetSignedUser = async (signer) => {
    if (signer == SIGNERS.Google) {
        const user = await GoogleSignin.getCurrentUser();

        return Promise.resolve(fromGoogleUserToUser(user, signer));
    } else {
        return Promise.reject(ERRORS.NO_SIGNER);
    }
}

export const IsSignedIn = async (signer) => {
    if (signer === SIGNERS.Google) {
        const is = await GoogleSignin.isSignedIn();
        if (is) {
            const user = await GoogleSignin.getCurrentUser();

            return Promise.resolve(FromGoogleUserToUser(user, signer));
        } else {
            return Promise.reject(ERRORS.NO_SIGNED_USER);
        }
    } else {
        return Promise.reject(ERRORS.NO_SIGNER);
    }
}

export const SignOut = async (signer) => {
    if (signer === SIGNERS.Google) {
        try {
            await GoogleSignin.revokeAccess();
            await GoogleSignin.signOut();

            return Promise.resolve(signer);
        } catch (error) {
            return Promise.reject(error);
        }
    } else {
        return Promise.reject(ERRORS.NO_SIGNER);
    }
}

export class SignInAction extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            signer: this.props.signer,

            onSignIn: this.props.onSignIn,
            onNotSigned: this.props.onNotSigned
        }
    }

    componentDidMount() {
        const { signer } = this.state;

        if (signer === SIGNERS.Google) {
            ConfigureGoogleSigner();
        }
    }

    getSignedUser() {
        const { signer, onSignIn, onNotSigned } = this.state;

        IsSignedIn(signer).then((user) => {
            if (onSignIn) {
                onSignIn(user);
            }
        }).catch((error) => {
            if (error === ERRORS.NO_SIGNED_USER) {
                if (onNotSigned) {
                    onNotSigned();
                }
            } else {
                if (!error in UNRAISABLE_ERRORS) {
                    console.error(error);
                }
            }
        });
    }

    signUserIn() {
        const { signer } = this.state;

        SignIn(signer).then(() => {
            this.getSignedUser();
        }).catch((error) => {
            if (!error in UNRAISABLE_ERRORS) {
                console.error('Erro de autenticação. Error: ' + error);
            }
        });
    }

    showGoogleSignInButton() {
        return (
            <Action icon={"google"} title={" Acessar com Google"}
                onPress={() => { this.signUserIn() }} />);
    }

    showFacebookSignInButton() {
        return (
            <Action icon={"facebook-square"} title=" Acessar com Facebook"
                onPress={() => {
                    Alert.alert(null, 'Ainda não implementado');
                }} />);
    }

    render() {
        const { signer } = this.state;

        if (signer === SIGNERS.Google) {
            return this.showGoogleSignInButton();
        } else if (signer === SIGNERS.Facebook) {
            return this.showFacebookSignInButton();
        } else {
            return null;
        }
    }

}

export class SignOutAction extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            signer: this.props.signer,

            onSignOut: this.props.onSignOut
        };
    }

    componentDidMount() {
        const { signer } = this.state;

        if (signer === SIGNERS.Google) {
            ConfigureGoogleSigner();
        }
    }

    signUserOut() {
        const { signer, onSignOut } = this.state;

        SignOut(signer).then(() => {
            if (onSignOut) {
                onSignOut(signer);
            }
        }).catch((error) => {
            if (!error in UNRAISABLE_ERRORS) {
                console.error('Erro de autenticação. Error: ' + error);
            }
        });
    }

    render() {
        return (
            <Action icon={"logout"} title=" Desconectar"
                onPress={() => { this.signUserOut() }} />);
    }

}

export class SignOptions extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const { onSignIn, onNotSigned, onSignOut } = this.props;
        const user = SyncStorage.get('user');

        if (user && !user.anonymous && onSignOut) {
            return (<SignOutAction signer={user.signer} onSignOut={onSignOut} />);
        } else {
            let key = 0;
            return (
                Object.values(SIGNERS).map((signer) => {
                    if (signer !== 'None') {
                        return (
                            <View key={++key}>
                                <SignInAction signer={signer} onSignIn={onSignIn} onNotSigned={onNotSigned} />
                                <VerticalSpacer />
                            </View>
                        );
                    }
                })
            );
        }
    }

}