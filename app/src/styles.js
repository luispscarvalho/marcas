import styled from 'styled-components/native';

export const Avatar = styled.Image`
    padding: 4px;
    width: 36px;
    height: 36px;
    border-radius: 18px;
`;

export const Logo = styled.Image`
    padding: 2px;
    transform: scale(0.5);
`;

export const SameRow = styled.View`
    flexDirection: row;
`;

export const CenterOfTheSameRow = styled.View`  
    flexDirection: row;
    justify-content: center;
    align-items: center;
`;

export const LeftOfTheSameRow = styled.View`
    flexDirection: row;
    justify-content: flex-start;
    align-items: flex-start;
`;

export const RightOfTheSameRow = styled.View`
    flexDirection: row;
    justify-content: flex-end;
    align-items: flex-end;
`;

export const SameColumn = styled.View`
    flexDirection: column;
`;

export const CenterOfTheSameColumn = styled.View`  
    flexDirection: column;
    justify-content: center;
    align-items: center;
`;

export const Padder = styled.View`
    padding: 10px;
    flexDirection: row;
`;

export const HorizontalSpacer = styled.View`
    marginHorizontal: 6;
`;

export const VerticalSpacer = styled.View`
    marginVertical: 6;
`;

export const Divider = styled.View`
    marginVertical: 5;
    marginHorizontal: 5;

    border-bottom-width: 1;
    borderColor: #DAE4D3;
`;

export const Loading = styled.ActivityIndicator.attrs({
    size: 'small',
    color: '#999',
})`
    margin: 30px 0;
`;

export const LikeStyle = {
    Color: '#f7567c',
    LikedByUserColor: '#f7567c',
    Size: 18
};

export const HeaderStyle = {
    marginTop: -10,
    backgroundColor: '#2b5eA3',
    justifyContent: 'space-evenly',
    borderBottomWidth: 0
}