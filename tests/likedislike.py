import unittest
from time import sleep


class TestLikeDislike(unittest.TestCase):

    def setUp(self):
        pass

    def getNumberOfLikes(self):
        likes = self.driver.find_element_by_accessibility_id("likes")
        numberOfLikes = int(likes.text)

        return numberOfLikes

    def test(self):
        product = self.driver.find_element_by_accessibility_id("feed 5e3a2dd2f633071cd9cd066a")
        product.click()
        product.click()
        sleep(2)

        iniValue = self.getNumberOfLikes()
        print('\nNumero de likes: ' + str(iniValue))

        likeButton = self.driver.find_element_by_accessibility_id("gostar")
        likeButton.click()
        sleep(2)

        newValue = self.getNumberOfLikes()
        print('\nNovo numero de likes: ' + str(newValue))
        self.assertIn(newValue, [iniValue - 1, iniValue + 1])

        dislikeButton = self.driver.find_element_by_accessibility_id("remover gostar")
        dislikeButton.click()
        sleep(2)

        newValue = self.getNumberOfLikes()
        print('\nNumero original de likes: ' + str(newValue))

        self.assertEqual(newValue, iniValue)
