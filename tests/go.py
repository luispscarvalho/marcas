from comment import TestComment
from likedislike import TestLikeDislike
from search import TestSearch

import unittest
from appium import webdriver
from time import sleep

# define capabilities + new driver
caps = {}
caps['platformName'] = 'Android'
caps['deviceName'] = '42008b3601c8455d'
caps['appPackage'] = 'com.melhoresmarcas'
caps['appActivity'] = '.MainActivity'
caps['noReset'] = True
driver = webdriver.Remote('http://localhost:4723/wd/hub', caps)

# func to execute test cases
def runTestCase(testCase, driver):
    testCase.driver = driver

    test = unittest.TestLoader().loadTestsFromTestCase(testCase)
    unittest.TextTestRunner(verbosity=2).run(test)

# running the testes

sleep(5)

# 1. search
# waiting up for 5 seconds might suffice for getting the app running
runTestCase(TestSearch, driver)

# 2. like/dislike
runTestCase(TestLikeDislike, driver)

# 3. + comment
runTestCase(TestComment, driver)

# all tests done! Tearing things down...
sleep(5)
driver.quit()