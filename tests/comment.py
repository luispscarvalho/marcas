import unittest
from appium.webdriver.common.touch_action import TouchAction
from time import sleep


class TestComment(unittest.TestCase):

    def setUp(self):
        pass

    def testAddComment(self):
        comments = self.driver.find_element_by_accessibility_id("comentarios")
        comments.click()
        sleep(2)

        addComment = self.driver.find_element_by_accessibility_id("novo comentario")
        addComment.click()
        sleep(2)

        comment = self.driver.find_element_by_accessibility_id("comentario")
        comment.send_keys("Novo comentario adicionado")

        send = self.driver.find_element_by_accessibility_id("enviar comentario")
        send.click()
        sleep(2)

        comment = self.driver.find_element_by_accessibility_id("seu comentario")
        self.assertEqual("Novo comentario adicionado", comment.text)

    def testDontRemoveComment(self):
        TouchAction(self.driver).press(x=974, y=339).wait(
            ms=1000).move_to(x=426, y=328).release().perform()
        sleep(2)

        removeComment = self.driver.find_element_by_accessibility_id("remover")
        removeComment.click()
        sleep(2)

        cancel = self.driver.find_element_by_id("android:id/button2")
        cancel.click()
        sleep(2)

        TouchAction(self.driver).press(x=486, y=303).move_to(x=465, y=900).release().perform()
        sleep(2)

        comment = self.driver.find_element_by_xpath(
            "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[1]")
        self.assertEqual("Novo comentario adicionado", comment.text)

    def testRemoveComment(self):
        TouchAction(self.driver).press(x=974, y=339).wait(
            ms=1000).move_to(x=426, y=328).release().perform()
        sleep(2)

        removeComment = self.driver.find_element_by_accessibility_id("remover")
        removeComment.click()
        sleep(2)

        confirm = self.driver.find_element_by_id("android:id/button1")
        confirm.click()
        sleep(2)

        comment = self.driver.find_element_by_xpath(
            "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView[2]")
        self.assertNotEqual("Novo comentario adicionado", comment.text)
