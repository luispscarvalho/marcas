import unittest
from time import sleep

class TestSearch(unittest.TestCase):

    def setUp(self):
        pass

    def test(self):
        searchInput = self.driver.find_element_by_accessibility_id("pesquisar por")
        searchInput.click()
        sleep(2)

        searchInput.send_keys("Maquiagem")
        searchButton = self.driver.find_element_by_accessibility_id('pesquisar')
        searchButton.click()
        sleep(2)

        product = self.driver.find_element_by_accessibility_id('produto Maquiagem 5e3a2dd2f633071cd9cd066a')
        self.assertIn("Maquiagem", product.text)
