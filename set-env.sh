#!/bin/sh
rm ./.env -f
rm ./app/environment.json -f

rm ./services/feed/URLS.py -f
rm ./services/like/URLS.py -f
rm ./services/comment/URLS.py -f
rm ./services/user/URLS.py -f
rm ./database/URLS.py -f

HOST_IP=$(ip -4 addr show wlp3s0 | grep -oP "(?<=inet ).*(?=/)")
ENV_VAR="HOST_IP=$HOST_IP"

API_VERSION="1.0"
ENV_JSON="{\"API_VERSION\": \"$API_VERSION\", \"HOST_IP\": \"$HOST_IP\"}"

IMAGE_SERVER_URL="IMAGES_SERVER_URL=\"http://$HOST_IP:9106/\""
VIDEO_SERVER_URL="VIDEOS_SERVER_URL=\"http://$HOST_IP:9106/\""

echo $ENV_VAR >> ./.env
echo $ENV_JSON >> ./app/environment.json

echo "DATABASE_HOST=\"$HOST_IP\"" >> ./database/URLS.py
echo "DATABASE_PORT=9105" >> ./database/URLS.py
echo $IMAGE_SERVER_URL >> ./database/URLS.py
echo $VIDEO_SERVER_URL >> ./database/URLS.py

DB_IP="DATABASE_HOST=\"172.28.1.5\""
DB_PORT="DATABASE_PORT=27017"

echo $DB_IP >> ./services/feed/URLS.py
echo $DB_PORT >> ./services/feed/URLS.py
echo $IMAGE_SERVER_URL >> ./services/feed/URLS.py
echo $VIDEO_SERVER_URL >> ./services/feed/URLS.py

echo $DB_IP >> ./services/like/URLS.py
echo $DB_PORT >> ./services/like/URLS.py
echo $IMAGE_SERVER_URL >> ./services/like/URLS.py
echo $VIDEO_SERVER_URL >> ./services/like/URLS.py

echo $DB_IP >> ./services/comment/URLS.py
echo $DB_PORT >> ./services/comment/URLS.py
echo $IMAGE_SERVER_URL >> ./services/comment/URLS.py
echo $VIDEO_SERVER_URL >> ./services/comment/URLS.py

echo $DB_IP >> ./services/user/URLS.py
echo $DB_PORT >> ./services/user/URLS.py
echo $IMAGE_SERVER_URL >> ./services/user/URLS.py
echo $VIDEO_SERVER_URL >> ./services/user/URLS.py

cp ./services/utils.py ./services/feed -f
cp ./services/utils.py ./services/like -f
cp ./services/utils.py ./services/comment -f
cp ./services/utils.py ./services/user -f

cp ./privatekey ./services/feed -f
cp ./privatekey ./services/like -f
cp ./privatekey ./services/comment -f
cp ./privatekey ./services/user -f